### BuffWatch

World of Warcraft addon to watch your current buffs and notify you on buff expired.

#### How to install

1. Put directory `BuffWatch` into the World of Warcraft directory:
```
C:/<path-to-wow>/Interface/AddOns
```

#### Bug reporting
You can report addon bugs to mrfrostikcz@gmail.com
