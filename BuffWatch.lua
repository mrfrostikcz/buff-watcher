local colors = require 'ansicolors'
local BuffWatch = CreateFrame('Frame')

BuffWatch.playerName = UnitFrame('player')
local enabled = true;

-- AddOn commands
CMD_BWATCH = '/bwatch';
local function MainCmd(cmd, self)
    if (cmd == 'enable') then
        enabled = true;
        DEFAULT_CHAT_FRAME:AddMessage('BuffWatch has been enabled.', 0, 1, 0);
    elseif (cmd == 'disable') then
        enabled = false;
        DEFAULT_CHAT_FRAME:AddMessage('BuffWatch has been disabled.', 1, 0, 0);
    else
        DEFAULT_CHAT_FRAME:AddMessage('Type `/bwatch enable` to activate BuffWatch or `/bwatch disable` to deactivate it.', 1, 0, 0)
    end
end
SlashCmdList['BWATCH'] = MainCmd;

BuffWatch:SetScript('OnEvent', function(...)
    local args = {... }

    if enabled == true then
        -- handle auras just for player himself
        if args[1] == "player" then
            local buffId, untilCancelled = GetPlayerBuff(0, "HELPFUL|HARMFUL|PASSIVE");
            -- TODO: Find buff by ID using API and make visualization effect
        end
    end
end)

BuffWatch:RegisterEvent('UNIT_AURA')

print(colors('%{redbg}[BuffWatch]%{reset}: Initialized. Use /bwatch for more options.'))
